#!/bin/bash
# Overwrite config with pristine config (only printers.conf and ppd/ should remain intact)
# This is the easiest way avoiding mapping printer.conf file and ppd/ directory separately.
cp -r /cups-config/* /etc/cups/
mkdir -p /etc/cups/ppd
chmod 777 /etc/cups/ppd

#Start cups
exec /usr/sbin/cupsd -f